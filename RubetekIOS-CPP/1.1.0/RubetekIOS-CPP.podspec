# **********************************************************************
# !!! This podspec file is used for prebuilt static framework located in other repo
# https://github.com/yklishevich/RubetekIOS-CPP-releases.git
# **********************************************************************

Pod::Spec.new do |spec|
    spec.name              			= 'RubetekIOS-CPP'
    spec.version           			= '1.1.0'
    spec.summary           			= 'C++ code moved out from RubetekEvo ios project into separate library, which allowed to lessen build tyme by about 1 min'
    spec.homepage          			= 'https://github.com/yklishevich/RubetekIOS-CPP-releases.git'            		
    spec.author            			= { 'Name' => 'eklishevich@gmail.com' }
    spec.license           			= { :type => "MIT", :file => "LICENSE.txt" }
    spec.platform          			= :ios
    spec.source            			= { :git => 'https://github.com/yklishevich/RubetekIOS-CPP-releases.git', :tag => spec.version }
    
    
    # module has header file that import "zip.h" directly without specifying framework name (it this way "libzip/zip.h"), so we add
    # to "HEADER_SEARCH_PATHS" path to headers of libzip dependency
    spec.user_target_xcconfig 		= { 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/libzip/libzip.framework/Headers" ' +
										'"${PODS_ROOT}/RubetekIOS-CPP/RubetekIOS-CPP.framework/Headers" ' +
										'"${PODS_ROOT}/RubetekIOS-CPP/RubetekIOS-CPP.framework/Headers/imc" ' +
										'"${PODS_ROOT}/RubetekIOS-CPP/RubetekIOS-CPP.framework/Headers/imc/cl" ' +
	 									'"${PODS_ROOT}/RubetekIOS-CPP/RubetekIOS-CPP.framework/Headers/libnet"' }

    spec.ios.deployment_target 		= '11.0'
    spec.vendored_frameworks		= 'RubetekIOS-CPP.framework'
    spec.source_files 				= 'RubetekIOS-CPP.framework/Versions/A/Headers/**/*.{h,hpp,ipp}'
	
	
	spec.dependency 'OpenSSL-Universal', '~> 1.0.2'
	
	# :source => 'https://bitbucket.org/rkksoft/com.gmail.rkksoft.podspecsrepo.git' 
	# (source must be specified when pushing podspec to spec repo right in `pod repo push` command)	
	spec.dependency 'boost', '~> 1.59.0.2' 
	
	# :source => 'https://bitbucket.org/rkksoft/com.gmail.rkksoft.podspecsrepo.git' 
	# (source must be specified when pushing podspec to spec repo right in `pod repo push` command)
	spec.dependency 'libzip', '~> 0.11.0'  		
	
	# :source => 'https://bitbucket.org/rkksoft/com.gmail.rkksoft.podspecsrepo.git' 
	# (source must be specified when pushing podspec to spec repo right in `pod repo push` command)
	spec.dependency 'thrift', '~> 1.0.0' 		
	
end
